FROM rocker/verse:4.2.0

# Install JAGS
RUN apt-get update
RUN apt-get install -y jags
 
# Install R packages
RUN R -e "install.packages(c('roxygen2', 'devtools'), dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('coda',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('deSolve',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('epitools',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('reshape2',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('rjags',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('zoo',dependencies=TRUE, repos='http://cran.rstudio.com/')"